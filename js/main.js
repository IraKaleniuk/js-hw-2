"use strict";

let userName = prompt("Enter your name:");
let userAge = prompt("Enter your age:");

if (userName === "" || userName === null || !isNaN(userName)
    || userAge === "" || userAge === null || isNaN(userAge)) {
    userName = prompt("Enter your name:", userName);
    userAge = prompt("Enter your age:", userAge);
}

if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge < 22) {
    confirm("Are you sure you want to continue?")
        ? alert(`Welcome, ${userName}!`)
        : alert("You are not allowed to visit this website!");
} else {
    alert(`Welcome, ${userName}!`);
}

